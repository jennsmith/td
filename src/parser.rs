/*! Parsers for texDown. */

use std::str::from_utf8 ;
use std::io::BufRead ;
use std::fmt::Display ;

use nom::{
  IResult, AsBytes, multispace, newline
} ;

use ast::{ SmplTxt, RichTxt, Frame, Frames } ;

/// Transforms some bytes into a string.
fn string_of_bytes(bytes: & [u8]) -> String {
  match from_utf8(bytes) {
    Ok(s) => s.lines().next().unwrap().to_string(),
    Err(e) => panic!(
      "could not convert bytes \"{:?}\" to string: {}", bytes, e
    )
  }
}


/// Parses anything but the characters in `but`.
fn any_but<S: AsBytes>(
  bytes: & [u8], but: S
) -> IResult<& [u8], & [u8]> {
  is_not!(bytes, but)
}

/// Parses some simple text.
fn smpl(bytes: & [u8]) -> IResult<& [u8], SmplTxt> {
  use ast::SmplTxt::* ;
  map!(
    bytes,
    many1!(smpl_txt_elm),
    |mut vec: Vec<SmplTxt>| match vec.len() {
      1 => vec.pop().unwrap(),
      _ => Seq(vec),
    }
  )
}

/** Parses simple text elements.

This parser is straightforward. It parses any inline formatting. As a
consequence of how the rich text parser works though, the beginning and the
ending of a format marker have to be on the same line.
*/
fn smpl_txt_elm(bytes: & [u8]) -> IResult<& [u8], SmplTxt> {
  use ast::SmplTxt::* ;
  use markers::simple::* ;
  alt!(
    bytes,
    // Bold.
    delimited!(
      tag!(bold_start),
      map!(
        apply!(any_but, bold_end),
        |content| Bold(string_of_bytes(content))
      ),
      tag!(bold_end)
    )
    | // Italic.
    delimited!(
      tag!(ita_start),
      map!(
        apply!(any_but, ita_end),
        |content| Ital(string_of_bytes(content))
      ),
      tag!(ita_end)
    )
    | // Code.
    delimited!(
      tag!(inline_code_start),
      map!(
        apply!(any_but, inline_code_end),
        |content| Code(string_of_bytes(content))
      ),
      tag!(inline_code_end)
    )
    | // Href.
    chain!(
      lhs: delimited!(
        tag!(href_lhs_start),
        map!(
          apply!(any_but, href_lhs_end),
          |content| string_of_bytes(content)
        ),
        tag!(href_lhs_end)
      ) ~
      rhs: delimited!(
        tag!(href_rhs_start),
        map!(
          apply!(any_but, href_rhs_end),
          |content| string_of_bytes(content)
        ),
        tag!(href_rhs_end)
      ),
      || Href(lhs, rhs)
    )
    | // Text.
    map!(
      apply!(
        any_but, reserved_start
      ), |content| Txt( string_of_bytes(content) )
    )
    | // Text.
    map!(
      apply!(
        any_but, reserved
      ), |content| Txt( string_of_bytes(content) )
    )
  )
}


/** Rich token indicating the start of a special, rich construction.

The idea is that the rich text parser (`rich_line`) parses each line to
construct a sequence of rich tokens. The rich tokens are then processed to
produce a rich text structure.
*/
#[derive(Debug)]
enum RichToken {
  /// An item from an enumeration.
  Item(usize, usize),
  /// Some simple text.
  Smpl(usize, usize, SmplTxt),
  /// Beginning of a code block.
  BeginCode(usize, usize, Option<String>),
  /// Line of code.
  CodeLine(usize, String),
  /// End of a code block.
  EndCode(usize, usize),
  /// Quote line.
  Quoted(usize, usize),
  /// Newline.
  NewLine(usize),
}
impl RichToken {
  /// Short string representation of a rich token.
  pub fn str_rep(& self) -> & 'static str {
    match * self {
      RichToken::Item(_,_) => "enumeration item",
      RichToken::Smpl(_,_,_) => "formatted text",
      RichToken::BeginCode(_,_,_) => "code block marker",
      RichToken::CodeLine(_,_) => "code line",
      RichToken::EndCode(_,_) => "code block marker",
      RichToken::Quoted(_, _) => "quoted line",
      RichToken::NewLine(_) => "newline",
    }
  }
  /// Indentation of a token.
  pub fn indent(& self) -> usize {
    match * self {
      RichToken::Item(_, indent) => indent,
      RichToken::Smpl(_, indent,_) => indent,
      RichToken::BeginCode(_, indent,_) => indent,
      RichToken::EndCode(_, indent) => indent,
      RichToken::Quoted(_, indent) => indent,
      RichToken::CodeLine(_, _) => 0,
      RichToken::NewLine(_) => 0,
    }
  }
  /// Line number of a token.
  pub fn line(& self) -> usize {
    match * self {
      RichToken::Item(line, _) => line,
      RichToken::Smpl(line, _,_) => line,
      RichToken::BeginCode(line, _,_) => line,
      RichToken::EndCode(line, _) => line,
      RichToken::Quoted(line, _) => line,
      RichToken::CodeLine(line, _) => line,
      RichToken::NewLine(line) => line,
    }
  }
  /// Returns true for `BeginCode` and `CodeLine` variants.
  pub fn in_code(& self) -> bool {
    match * self {
      RichToken::BeginCode(_,_,_) |
      RichToken::CodeLine(_,_) => true,
      _ => false
    }
  }
}


/// Parser an identifier.
fn ident_parser(bytes: & [u8]) -> IResult<& [u8], String> {
  chain!(
    bytes,
    left: one_of!("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ") ~
    right: is_a!(
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_"
    ),
    || {
      format!("{}{}", left, string_of_bytes(right))
    }
  )
}

/// Parses a code line, or the end of a code block.
fn code_line_parser(bytes: & [u8], line: usize) -> IResult<& [u8], RichToken> {
  use markers::rich::block_code_end ;
  let mut count = 0 ;
  alt!(
    bytes,
    chain!(
      many0!(
        alt!(
          map!(char!(' '), |_| count += 1) |
          map!(char!('\t'), |_| count += 2)
        )
      ) ~
      tag!(block_code_end) ~
      opt!(multispace),
      || RichToken::EndCode(line, count)
    ) |
    chain!(
      bytes: is_not!("\n\r") ~
      newline,
      || RichToken::CodeLine( line, string_of_bytes(bytes) )
    )
  )
}


/// Appends some code lines or code block end to a vector of tokens.
fn code_line(
  bytes: & [u8], line: usize, vec: & mut Vec<RichToken>
) -> () {
  match code_line_parser(bytes, line) {
    IResult::Done(rest, res) => {
      debug_assert!(rest.is_empty()) ;
      vec.push(res)
    },
    _ => unreachable!(),
  }
}


/// Parses an indentation, an optional rich text token, and an optional simple
/// text.
fn rich_pref(
  bytes: & [u8], line: usize
) -> IResult<& [u8], (Option<RichToken>, Option<RichToken>)> {
  use markers::rich::* ;
  let mut count = 0 ;
  chain!(
    bytes,
    many0!(
      alt!(
        map!(char!(' '), |_| count += 1) |
        map!(char!('\t'), |_| count += 2)
      )
    ) ~
    res: alt!(
      chain!(
        tag!(block_code_start) ~
        language: opt!(
          alt!(
            ident_parser |
            delimited!( char!('['), ident_parser, char!(']') )
          )
        ),
        || (
          Some( RichToken::BeginCode(line, count, language) ), None
        )
      ) |
      chain!(
        token: alt!(
          map!(
            tag!(enum_item), |_| {
              let res = RichToken::Item(line, count) ;
              count += enum_item.len() ;
              res
            }
          ) |
          map!(
            tag!(quote_line), |_| {
              let res = RichToken::Quoted(line, count) ;
              count += quote_line.len() ;
              res
            }
          )
        ) ~
        opt!( map!(multispace, |bytes: & [u8]| count += bytes.len()) ) ~
        txt: opt!( map!(smpl, |txt| RichToken::Smpl(line, count, txt)) ),
        || (Some(token), txt)
      ) |
      map!( smpl, |txt| (None, Some( RichToken::Smpl(line, count, txt) )) )
    ) ~
    opt!(multispace),
    || res
  )  
}


/// Parses some a line as a vector of rich tokens.
fn rich_line(
  bytes: & [u8], line: usize, vec: & mut Vec<RichToken>
) -> Result<(), String> {
  if bytes.len() == 1 {
    vec.push(RichToken::NewLine(line))
  } else {
    match rich_pref(bytes, line) {
      IResult::Done(rest, res) => if rest.is_empty() {
        match res {
          (None, None) => vec.push(RichToken::NewLine(line)),
          (tkn, txt) => {
            match tkn { Some(tkn) => vec.push(tkn), None => () } ;
            match txt { Some(txt) => vec.push(txt), None => () } ;
          },
        }
      } else {
        return Err(
          format!(
            "error on line {}\n> \"{}\"\n\
            > expected nothing, after {:?}, got \"{}\"",
            line, string_of_bytes(bytes), vec[vec.len() - 1],
            string_of_bytes(rest)
          )
        )
      },
      e => return Err(
        format!(
          "error on line {}\n> \"{}\"> {:?}",
          line, string_of_bytes(bytes), e
        )
      ),
    }
  } ;
  Ok(())
}


/// Returns true if the last element of a vector of tokens indicates we're
/// parsing a code block.
fn in_code(tokens: & Vec<RichToken>) -> bool {
  match tokens.last() {
    None => false,
    Some(last) => last.in_code(),
  }
}

/** Creates a vector of `RichToken`s from a `BufRead`.

Reads line by line.
*/
fn rich_tokens_of<Reader: BufRead>(
  mut reader: Reader
) -> Result<Vec<RichToken>, String> {
  // Stores the result.
  let mut vec = Vec::with_capacity(100) ;
  // Buffer to read each line.
  let mut buffer = String::with_capacity(100) ;
  // Line count.
  let mut line = 0 ;

  loop {
    buffer.clear() ;
    line += 1 ;
    match reader.read_line(& mut buffer) {
      // Reading nothing.
      Ok(0) => break,
      // Reading something.
      Ok(_) => if ! in_code(& vec) {
        try!(
          rich_line(
            buffer.as_bytes(), line, & mut vec
	  )
        )
      } else {
        code_line(buffer.as_bytes(), line, & mut vec)
      },
      // Error.
      Err(e) => println!("read_line error: {}", e),
    }
  } ;

  vec.shrink_to_fit() ;

  Ok(vec)
}

/// Turns a vector of rich text in a rich text variant. Adds it to the
/// previous item in the stack if any, to [vec] otherwise.
fn close_block_and_add(
  mut block: Vec<RichTxt>,
  stack: & mut Vec<(usize, usize, Vec<RichTxt>, RichTxt)>,
  vec: & mut Vec<RichTxt>
) {
  let rich = if block.len() == 1 {
    block.pop().unwrap()
  } else {
    RichTxt::Seq(block)
  } ;

  // Is there a block above?
  if let Some((l, i, b, above)) = stack.pop() {
    // Add the block we just closed.
    stack.push( (l, i, b, above.add(rich)) )
  } else { // We're at the top level.
    // Add the block we just closed to result sequence.
    vec.push(rich)
  }
}


/// Transforms a list of tokens into some rich text.
fn rich_txt_of(tokens: & mut Vec<RichToken>) -> Result<RichTxt, String> {
  /* Construction stack. Stack elements are 4-tuples, understood as *blocks*
  storing
  - the line where the block started
  - the indentation of the block
  - the rich text elements constructed for this block so far
  - the rich text element currently being constructed.
  */
  let mut stack: Vec<(
    usize, usize, Vec<RichTxt>, RichTxt
  )> = Vec::with_capacity(100) ;
  // Final sequence of rich text.
  let mut vec: Vec<RichTxt> = Vec::with_capacity(100) ;

  loop {
    match tokens.pop() {

      // Beginning of an item?
      Some( RichToken::Item(line, indent) ) => {

        match stack.pop() {
          Some( (block_line, block_indent, mut block, rich) ) => {
            if indent < block_indent {
              // We are closing this block.
              block.push(rich) ;
              close_block_and_add(block, & mut stack, & mut vec) ;
              // Looping until we get to the right indentation.
              tokens.push( RichToken::Item(line, indent) )
            } else if indent > block_indent {
              // Creating a new enumeration.
              stack.push( (block_line, block_indent, block, rich) ) ;
              stack.push( (line, indent, vec![], RichTxt::Enum( vec![] )) )
            } else {
              // We are at the right indentation.
              match rich {
                RichTxt::Enum(mut items) => {
                  // We are adding a new item.
                  items.push(RichTxt::Seq( vec![] )) ;
                  stack.push(
                    (block_line, block_indent, block, RichTxt::Enum(items))
                  )
                },
                rich => {
                  stack.push( (block_line, block_indent, block, rich) ) ;
                  // We are creating a new enumeration.
                  stack.push(
                    (block_line, block_indent, vec![], RichTxt::Enum( vec![] ))
                  )
                },
              }
            }
          },
          None => {
            // Creating new enumeration.
            stack.push( (line, indent, vec![], RichTxt::Enum( vec![] )) )
          },
        }
      },

      // Simple text?
      Some( RichToken::Smpl(line, indent, txt) ) => {

        match stack.pop() {
          Some( (block_line, block_indent, mut block, rich) ) => {
            if indent < block_indent {
              block.push(rich) ;
              // We are closing this block.
              close_block_and_add(block, & mut stack, & mut vec)
            } else if indent == block_indent {
              block.push(rich) ;
              // Adding to current block.
              stack.push(
                (block_line, block_indent, block, RichTxt::Smpl(txt))
              )
            } else {
              stack.push(
                (block_line, block_indent, block, rich)
              ) ;
              stack.push(
                (line, indent, vec![], RichTxt::Smpl(txt))
              )
            }
          },

          None => {
            // Adding to stack.
            stack.push( (line, indent, vec![], RichTxt::Smpl(txt)) )
          },
        }

      },
      Some( RichToken::BeginCode(line, indent, lang) ) => {
	// creating a mutable vector to contain the unedited code lines
 	let mut code_lines = vec![];
	// Loop to look through tokens to find the codelines
	'code_loop: loop {
	  match tokens.pop() {
	    // if it's a codeline, add it to the vetor
	    Some( RichToken::CodeLine(line, code_line)) => {
	      code_lines.push(code_line);
  	    },
	    // if it's the endcode token, it ends the loop
	    Some( RichToken::EndCode( line, indent)) => {
              break 'code_loop
            },
	    Some( tkn ) => return Err(
	      format!(
		"Expected code line or end of code block, started line {}, but got {:?}.", line, tkn
	      )
            ),
	    // If BeginCode is seen, and isn't followed by either CodeLine or EndCode it shows an error
            None => return Err(
	      format!(
	        "Expected end of code block started line {}, but got end of frame.", line
	      )
            ),
          }
        };
	// creates a rich vector of the codelines
        let rich = RichTxt::Code(lang, code_lines);
	// push the vector of rich codelines onto the stack
//        stack.push((line, indent, code_lines, rich));
        panic!("
	  // check indentation
	  match stack.pop() {
	    Some((bline, bindent, mut block, rich)) => {
	      if indent < bindent {
		block.push(rich);
		// close the block
		close_block_and_add(block, & mut, rich)
	      } else if indent == bindent {
		block.push(rich);
		stack.push((bline, bindent, block, RichTxt:Code(lang, code_lines)))
	      } else {
		stack.push(bline, bindent, block, rich)
	      };
	      stack.push(line, indent, vec![], RichTxt::Code(lang, code_lines))
	    }
	  }
	")	
      },
      Some( RichToken::CodeLine(_line, _code_line) ) => {
        unreachable!() 
      },
      Some( RichToken::EndCode(_line, _indent) ) => {
        unreachable!()
      },
      Some( RichToken::Quoted(_line, _indent) ) => {
        unimplemented!()
      },
      Some( RichToken::NewLine(_line) ) => {
        close_block_and_add( vec![ RichTxt::NewLine ], & mut stack, & mut vec )
      },
      None => {
        while let Some( (_, _, mut block, rich) ) = stack.pop() {
          block.push(rich) ;
          close_block_and_add(block, & mut stack, & mut vec)
        } ;
        vec.shrink_to_fit() ;
        return Ok(
          if vec.len() == 1 { vec.pop().unwrap() } else {
            RichTxt::Seq(vec)
          }
        )
      },
    }
  }
}


/// Removes trailing newlines from a vector of rich tokens.
fn rm_nl(tokens: & mut Vec<RichToken>) {
  loop {
    match tokens.last() {
      Some( & RichToken::NewLine(_) ) => (),
      _ => break,
    }
    tokens.pop() ;
  }
}

/** Creates a frame from some rich tokens. **Assumes** the tokens are in
reverse order (pop semantics).

Title is all trailing simple text with indentation 0.

Body is everything after that with indentation non-0.

Stops as soon as a token with indentation 0 after the body is found.
*/
fn frame(tokens: & mut Vec<RichToken>) -> Result<Frame, String> {
  // Retrieve title.
  let title = match tokens.pop() {
    Some( RichToken::Smpl(line, indent, txt) ) => if indent == 0 { txt } else {
      return Err(
        format!(
          "Frame parser error line {}: expected title, got indented text", line
        )
      )
    },
    Some(token) => return Err(
      format!(
        "Frame parser error line {}: expected title, got {}",
        token.line(), token.str_rep()
      )
    ),
    None => return Err(
      "Frame parser error: expected title but got nothing".to_string()
    ),
  } ;

  rm_nl(tokens) ;

  let mut vec = Vec::with_capacity(10) ;
  loop {
    match tokens.pop() {
      Some(
        nl @ RichToken::NewLine(_)
      ) => vec.push(nl),
      Some(token) => if token.indent() > 0 {
        vec.push(token)
      } else {
        tokens.push(token) ;
        break
      },
      None => break,
    }
  } ;
  vec.reverse() ;

  // Retrieve body.
  let body = try!( rich_txt_of(& mut vec) ) ;

  Ok( Frame::mk( title, body ) )
}


/// Creates a list of frames from some rich tokens.
fn frames(mut tokens: Vec<RichToken>) -> Result<Frames, String> {
  tokens.reverse() ;

  let mut vec = Vec::with_capacity(100) ;

  rm_nl(& mut tokens) ;

  while ! tokens.is_empty() {
    vec.push(
      try!( frame(& mut tokens) )
    ) ;
    rm_nl(& mut tokens) ;
  } ;

  Ok( Frames::mk(vec) )
}

/// Creates a list of frames from a file.
pub fn frames_of<Reader: BufRead, Source: Display>(
  reader: Reader, verbose: bool, from: Source
) -> Result<Frames, String> {
  if verbose {
    println!("|===| Parsing \"{}\"", from) ;
  } ;

  let tokens = try!( rich_tokens_of(reader) ) ;
  // println!("done parsing:") ;
  // for token in tokens.iter() {
  //   println!("> {:?}", token)
  // } ;

  if verbose {
    println!("| done tokenizing lines") ;
  } ;

  let frames = try!( frames(tokens) ) ;

  if verbose {
    println!("| done generating frames") ;
    println!("|===|") ;
    println!("")
  }

  Ok(frames)
}

/// Creates a `Frames` from a file (`source`).
pub fn frames_of_file(source: & str, verbose: bool) -> Result<Frames, String> {
  use std::io::{ Read, BufReader } ;
  use std::fs::OpenOptions ;

  let file = match OpenOptions::new().read(true).open(source) {
    Ok(file) => file,
    Err(e) => return Err(
      format!("Error opening file \"{}\":\n> {}", source, e)
    ),
  } ;
  let mut reader = BufReader::new(file) ;

  frames_of(& mut reader, verbose, source)
}
