//! Skeleton crate for a texdown to AST project.

// Parser library.
#[macro_use]
extern crate nom ;

pub mod markers ;
pub mod ast ;
pub mod parser ;

macro_rules! try_exit {
  ($e:expr) => (
    match $e {
      Ok(res) => res,
      Err(err) => {
        use std::process::exit ;
        println!("") ;
        for line in err.lines() {
          println!("{}", line)
        } ;
        println!("") ;
        exit(2)
      }
    }
  ) ;
}

fn main() {
  use ast::Frames ;

  println!("") ;

  // let ast = Frame::mk(
  //   SmplTxt::Seq(
  //     vec![
  //       "This is the ".txt(),
  //       "title".bold()
  //     ]
  //   ),
  //   RichTxt::Enum(
  //     vec![
  //       "item 1".bold().as_rich(),
  //       "item 2".code().as_rich(),
  //       "item 3".ital().as_rich(),
  //       "item 4".href("www.duckduckgo.com").as_rich()
  //     ]
  //   ),
  // ) ;

  // ast.to_file("test.td").unwrap() ;

  // println!("Created an AST!") ;
  // println!("") ;

  let source = {
    use std::env::args ;
    let mut args = args() ;
    // Skipping first argument, it's the program's name.
    let _ = args.next() ;
    match args.next() {
      Some(source) => source,
      None => "rsc/test.td".to_string(),
    }
  } ;
  let frames = try_exit!( Frames::of_file(& source, true) ) ;

  let target = "output.td" ;
  println!("|===| Dumping presentation in texdown") ;
  println!("| to file \"{}\"", target) ;

  frames.to_texdown(target).unwrap() ;

  println!("| done") ;
  println!("|===|") ;
  println!("")
}