//! Converts an AST to texDown.

use std::io::{ Write, Result } ;

use markers::simple::* ;
use markers::rich::* ;

/// Provides a function writing `Self` in a writer in texdown format.
pub trait ToTexDown {
  /// Writes an `& self` in texDown in a writer.
  fn to_tex_down<Writer: Write>(& self, w: & mut Writer) -> Result<()> ;
}

/// Provides a function writing `Self` in a writer in texdown format, with
/// identation.
pub trait ToTexDownIndent {
  /** Writes an `& self` in texDown in a writer.
  
  Parameter `indent` gives the indentation to use for all lines **but the
  first one**.
  */
  fn to_tex_down<Writer: Write>(
    & self, w: & mut Writer, indent: & str
  ) -> Result<()> ;
}


impl ToTexDown for ::ast::SmplTxt {
  fn to_tex_down<Writer: Write>(& self, w: & mut Writer) -> Result<()> {
    use ::ast::SmplTxt::* ;

    match * self {
      Seq(ref vec) => {
        for smpl_txt in vec.iter() {
          // Could stack overflow, but the depth of the AST is unlikely to be
          // high enough.
          try!( smpl_txt.to_tex_down(w) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      Txt(ref txt) => write!(
        w, "{}", txt
      ),
      Bold(ref txt) => write!(
        w, "{}{}{}", bold_start, txt, bold_end
      ),
      Ital(ref txt) => write!(
        w, "{}{}{}", ita_start, txt, ita_end
      ),
      Code(ref txt) => write!(
        w, "{}{}{}", inline_code_start, txt, inline_code_end
      ),
      Href(ref lhs, ref rhs) => write!(
        w, "{}{}{}{}{}{}",
        href_lhs_start, lhs, href_lhs_end,
        href_rhs_start, rhs, href_rhs_end
      ),
    }

  }
}


impl ToTexDownIndent for ::ast::RichTxt {
  fn to_tex_down<Writer: Write>(
    & self, w: & mut Writer, indent: & str
  ) -> Result<()> {
    use ::ast::RichTxt::* ;

    match * self {
      Smpl(ref smpl_txt) => {
        try!( smpl_txt.to_tex_down(w) ) ;
        write!(w, "\n")
      },
      Seq(ref vec) => {
        let mut fst = true ;
        for rich in vec.iter() {
          // Could stack overflow, but the depth of the AST is unlikely to be
          // high enough.
          if fst { fst = false } else {
            try!( write!(w, "{}", indent) )
          } ;
          try!( rich.to_tex_down(w, indent) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      Enum(ref items) => {
        let mut fst = true ;
        for item in items.iter() {
          if fst {
            try!( write!(w, "{} ", enum_item) ) ;
            fst = false
          } else {
            try!( write!(w, "{}{} ", indent, enum_item) ) ;
          } ;
          // Could stack overflow, but the depth of the AST is unlikely to be
          // high enough.
          try!( item.to_tex_down(w, & format!("{}  ", indent)) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      Code(ref language, ref code_lines) => {
        try!( write!(w, "{}", block_code_start) ) ;
        match * language {
          None => (),
          Some(ref language) => try!( write!(w, "[{}]", language) ),
        } ;
        try!( write!(w, "\n") ) ;
        for line in code_lines {
          try!( write!(w, "{}\n", line) )
        } ;
        write!(w, "{}{}\n", indent, block_code_end)
      },
      Quote(ref rich_txt) => {
        let mut string: Vec<u8> = Vec::with_capacity(103) ;
        try!( rich_txt.to_tex_down(& mut string, & indent) ) ;
        try!( write!(w, "\n") ) ;
        for line in String::from_utf8(string).unwrap().lines() {
          try!( write!(w, "{}> {}\n", indent, line) ) ;
          try!( w.flush() )
        } ;
        Ok(())
      },
      NewLine => write!(w, "\n"),
    }

  }
}


impl ToTexDown for ::ast::Frame {
  fn to_tex_down<Writer: Write>(& self, w: & mut Writer) -> Result<()> {
    try!( self.title.to_tex_down(w) ) ;
    try!( write!(w, "\n\n  ") ) ;
    try!( self.body.to_tex_down(w, "  ") ) ;
    write!(w, "\n")
  }
}